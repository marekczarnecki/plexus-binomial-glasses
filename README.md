# plexus-binomial-glasses

This file contains usage instructions, design outline and working notes made during development.

## Usage

The code can be built, tests run and code coverage reports generated by running:

~~~
mvn clean install cobertura:cobertura
~~~

Coverage reports are then visible in ./target/site/cobertura/index.html

After a build has completed the utility can be run from the shell, e.g.

~~~
java -jar plexus-binomial-glasses-0.0.1-SNAPSHOT.one-jar.jar 2 1 1.15
~~~


## General Design

Initial observation is the problem is almost verbatim Pascal's Triangle, which implies a solution as a single mathematical function is possible. Probably this would lead to a monolith which would be very poor design - but much more efficient !

The core static object is a Glass, and these implement the Observer pattern to allow flows of water to be received and then distributed.

This is handled flow by flow. So the top glass is notified of ONE flow of water, and distributes to two observers. 

Glasses in subsequent rows receive either one or two flows of water, and distribute to two observers.

Interrogation to find the volume in a given glass will be done on the gross structure of Glasses, which will be a  List<List<Glass>>

## Working Notes

2019-03-29 18.13: Work began with the setup of the maven script and the Eclipse project to allow IDE to work seamlessly.

2019-03-29 18:24 The core static object is 'Glass' - all the rest is just configurations of these objects and allocating the inputs and outputs.

2019-03-29 18.55 Decided to implement the structural connections (overflow paths for water) as an Observer pattern. So Glass extends Observer and gets a notify method (receiveWater)

2019-03-29 19.04 Momentary panic when I thought I would have to build all flows in to a glass before notifying the observers. But this doesn't matter - the distribution rule is the same if I add all the received volumes together or distribute the flows one by one. Just have to keep the volume received, the capacity and (maybe) the unallocated volume.

2019-03-29 19.35 Finished coding of the distribution of water method (receiveWater) Now developing tests to see how BigDecimal works. Also figured out I just need to store 'maxCapacity' and 'currentCapacity' to code the distribution to observers.

2019-03-29 19.51 Successful test of three glass pyramid. Need to code for null observers in tree. 

2019-03-29 19.52 If I change the constructor of the Glass to take a reference to its 'Pyramid' then the construction can be abstracted out to the Factory, the Glass can be used in the main() to get the Pyramid and the Pyramid can handle returning results for a specific glass(x,y)

2019-03-29 20.32 Initial test of filing a three row pyramid to get a binomial distribution (100, 200, 100) at base failed. Debugging. 

2019-03-29 20.41 Solved the fill with water problem - the reference to the previous row wasn't being updated.

2019-03-29 20.42 Just need to complete a main interface and check code coverage. I think...

2019-03-29 20.58 Gave the command line a simple interface (three numbers) results matched test!

2019-03-29 20.58 Just check code coverage.

2019-03-29 21.08 Finished. Coverage at 95%. 

## References

[1] https://en.wikipedia.org/wiki/Binomial_distribution

[2] https://en.wikipedia.org/wiki/Observer_pattern



