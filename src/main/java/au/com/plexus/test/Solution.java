package au.com.plexus.test;

import java.math.BigDecimal;

import au.com.plexus.test.model.Pyramid;

public class Solution {

	public static BigDecimal currentVolume;

	public static void main(String[] args) {
		if (args == null || args.length != 3) {
			System.out.println("Usage: au.com.plexus.test.Solution <row_zero_based> <glass_zero_based> <volume in litres>");
			System.out.println("Result is in milliliters");
			
			return;
		}

		int row = Integer.parseInt(args[0]);
		int glass = Integer.parseInt(args[1]);
		
		BigDecimal volume = new BigDecimal(args[2]);
		volume = volume.multiply(new BigDecimal(1000));
		
		if (row < 0 || glass < 0 || glass > row || BigDecimal.ZERO.compareTo(volume) > 0) {
			System.out.println("Invalid parameters");
		}
		
		Pyramid pyramid = new Pyramid(row + 1);
		
		pyramid.receiveWater(volume);
		
		currentVolume = pyramid.getCurrentVolume(row, glass);
		
		System.out.println(currentVolume);
	}

}
