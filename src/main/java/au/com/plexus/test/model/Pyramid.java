package au.com.plexus.test.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import au.com.plexus.test.Factory;

public class Pyramid implements Observer{
	
	private List<List<Glass>> pyramid = new ArrayList<List<Glass>> ();

	public Pyramid (int numberOfRows) {
		List<Glass> previousRow = null;
		
		for (int row = 0; row < numberOfRows; row++) {
			List<Glass> currentRow = new ArrayList<Glass> ();
			pyramid.add(currentRow);
			
			for(int column = 0; column <= row; column++) {
				Glass glass = Factory.createGlass();
				currentRow.add(glass);
				
				if (previousRow != null) {
					Glass previousLeft = null;
					Glass previousRight = null;
					
					if (column > 0) {
						previousLeft = previousRow.get(column - 1);
					}
					
					if (column < previousRow.size()) {
						previousRight = previousRow.get(column);
					}
					
					if (previousLeft != null) {
						previousLeft.registerRightObserver(glass);
					}
					
					if (previousRight != null) {
						previousRight.registerLeftObserver(glass);
					}
				}
			}
			
			previousRow = currentRow;
		}
	}
	
	public BigDecimal getCurrentVolume (int i, int j) {
		List<Glass> row = pyramid.get(i);
		Glass glass = row.get(j);
		
		return glass.getCurrentVolume();
	}

	@Override
	public void receiveWater(BigDecimal volume) {
		List<Glass> row = pyramid.get(0);
		Glass glass = row.get(0);

		glass.receiveWater(volume);
	}
}
