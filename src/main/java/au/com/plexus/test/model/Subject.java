package au.com.plexus.test.model;

public interface Subject {

	public void registerLeftObserver(Glass glass);

	public void registerRightObserver(Glass glass);

}
