package au.com.plexus.test.model;

import java.math.BigDecimal;

public class GlassImpl implements Glass {
	
	private static final BigDecimal TWO = new BigDecimal(2);
	
	private final BigDecimal maxVolume;
	
	private BigDecimal currentVolume;

	Glass leftObserver;
	Glass rightObserver;

	public GlassImpl(BigDecimal capacity) {
		super();
		this.maxVolume = capacity;
		this.currentVolume = BigDecimal.ZERO;
	}
	
	@Override
	public void registerLeftObserver(Glass glass) {
		leftObserver = glass;
	}

	@Override
	public void registerRightObserver(Glass glass) {
		rightObserver = glass;
	}

	@Override
	public BigDecimal getMaxVolume() {
		return maxVolume;
	}

	@Override
	public BigDecimal getCurrentVolume() {
		return currentVolume;
	}
	
	@Override
	public void receiveWater(BigDecimal volume) {
		this.currentVolume = currentVolume.add(volume);
		
		if (currentVolume.compareTo(maxVolume) > 0) {
			BigDecimal overflow = currentVolume.subtract(maxVolume);
			currentVolume = maxVolume;
			
			if (leftObserver != null) {
				leftObserver.receiveWater(overflow.divide(TWO));
			}
			
			if (rightObserver != null) {
				rightObserver.receiveWater(overflow.divide(TWO));
			}
		}
	}

}
