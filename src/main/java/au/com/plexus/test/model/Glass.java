package au.com.plexus.test.model;

import java.math.BigDecimal;

public interface Glass extends Subject, Observer {
	
	public BigDecimal getMaxVolume();
	
	public BigDecimal getCurrentVolume();
	
}
