package au.com.plexus.test.model;

import java.math.BigDecimal;

public interface Observer {
	
	public void receiveWater (BigDecimal volume);

}
