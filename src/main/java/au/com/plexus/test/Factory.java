package au.com.plexus.test;

import java.math.BigDecimal;

import au.com.plexus.test.model.Glass;
import au.com.plexus.test.model.GlassImpl;
import au.com.plexus.test.model.Pyramid;

public class Factory {
	
	public static final BigDecimal CAPACITY = new BigDecimal(250);
	
	public static Glass createGlass() {
		return new GlassImpl(CAPACITY);
	}
	
	public static Pyramid createPyramid(int numberOfRows) {
		return new Pyramid(numberOfRows);
	}

}
