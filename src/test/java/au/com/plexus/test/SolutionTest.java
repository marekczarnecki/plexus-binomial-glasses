package au.com.plexus.test;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SolutionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void canRunMainWithNullArguments() {
		Solution.main(null);
	}

	@Test
	public void verifyCurrentVolumeInMiddleBaseOfThreeRowPyramid() {
		Solution.main(new String[] {"2", "1", "1.150"});
		
		assertTrue(Solution.currentVolume.compareTo(new BigDecimal(200)) == 0);
	}

}
