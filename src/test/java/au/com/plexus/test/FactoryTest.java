package au.com.plexus.test;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.plexus.test.model.Glass;

public class FactoryTest {

	private static Factory factory;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		factory = new Factory();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateGlass() {
		Glass glass = factory.createGlass();
		
		assertNotNull(glass);
	}

}
