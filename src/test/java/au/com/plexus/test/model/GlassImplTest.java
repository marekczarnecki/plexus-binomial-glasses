package au.com.plexus.test.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.plexus.test.Factory;

public class GlassImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void canConstructAGlass() {
		Glass glass = new GlassImpl(Factory.CAPACITY);
		
		assertNotNull(glass);
	}

	@Test
	public void nearlyFillAGlass() {
		Glass glass = new GlassImpl(Factory.CAPACITY);
		assertNotNull(glass);
		
		glass.receiveWater(new BigDecimal (100));
		assertTrue(glass.getCurrentVolume().compareTo(new BigDecimal (100)) == 0);
	}

	@Test
	public void fillAndTestAPyramidOfThreeGlasses() {
		GlassImpl glassTop = new GlassImpl(Factory.CAPACITY);
		Glass glassLeft = new GlassImpl(Factory.CAPACITY);
		Glass glassRight = new GlassImpl(Factory.CAPACITY);
		assertNotNull(glassTop);
		assertNotNull(glassLeft);
		assertNotNull(glassRight);
		
		glassTop.registerLeftObserver(glassLeft);
		glassTop.registerRightObserver(glassRight);
		
		glassTop.receiveWater(new BigDecimal (450));
		assertTrue(glassTop.getCurrentVolume().compareTo(new BigDecimal (250)) == 0);
		
		assertTrue(glassLeft.getCurrentVolume().compareTo(new BigDecimal(100)) == 0);
		assertTrue(glassRight.getCurrentVolume().compareTo(new BigDecimal(100)) == 0);
	}
}
