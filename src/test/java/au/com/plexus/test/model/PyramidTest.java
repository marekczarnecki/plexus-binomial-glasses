package au.com.plexus.test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PyramidTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void buildAOneRowPyramid() {
		Pyramid pyramid = new Pyramid(1);
		
		assertNotNull(pyramid);
	}

	@Test
	public void buildAOneRowPyramidAndCheckCurrentVolumeIsZero() {
		Pyramid pyramid = new Pyramid(1);
		assertNotNull(pyramid);
		
		assertEquals(pyramid.getCurrentVolume(0, 0), new BigDecimal(0));
	}

	@Test
	public void buildATwoRowPyramidAndCheckCurrentVolumeIsZero() {
		Pyramid pyramid = new Pyramid(2);
		assertNotNull(pyramid);

		assertEquals(pyramid.getCurrentVolume(1, 1), new BigDecimal(0));
	}

	@Test
	public void buildAThreeRowPyramidFillWithWaterAndCheckCurrentVolumesAtBase() {
		Pyramid pyramid = new Pyramid(3);
		assertNotNull(pyramid);
		
		//pour the amount of water to fill level 3 in a binomial distribution
		pyramid.receiveWater(new BigDecimal(250 + 250 + 250 + 100 + 200 + 100));

		// top level (0) filled to capacity
		assertEquals(pyramid.getCurrentVolume(0, 0), new BigDecimal(250));

		// next level (1) filled to capacity
		assertEquals(pyramid.getCurrentVolume(1, 0), new BigDecimal(250));
		assertEquals(pyramid.getCurrentVolume(1, 1), new BigDecimal(250));

		// next level (2) filled to binomial values
		assertEquals(pyramid.getCurrentVolume(2, 0), new BigDecimal(100));
		assertEquals(pyramid.getCurrentVolume(2, 1), new BigDecimal(200));
		assertEquals(pyramid.getCurrentVolume(2, 2), new BigDecimal(100));
	}

}
